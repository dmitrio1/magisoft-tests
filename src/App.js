import React from 'react';
import './App.css';
import Router from './router';
import Home from './pages/Home';

const App = () => {
  return (
    <div className="App">
      <Home />
      <Router />
    </div>
  );
};

export default App;
