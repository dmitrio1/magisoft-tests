import React from 'react';
// import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
/* PLOP_INJECT_COMPONENT_IMPORT */
import RegisterForm from 'src/pages/RegisterForm';
import Authorization from 'src/pages/AuthorizationForm';

const Router = () => {
  return (
    <div>
      <Switch>
        <Route path="/user-register" component={RegisterForm} />
        <Route path="/user-authentication" component={Authorization} />
      </Switch>
    </div>
  );
};

// Router.propTypes = {};

export default Router;
