import React from 'react';

import styles from './register.module.scss';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';

const RegisterForm = () => {
  return (
    <form className={styles.root}>
      <p className={styles.text}>Registration</p>
      <div className={styles.formInput}>
        <Input placeholder="Name" />
        <Input placeholder="Surname" />
        <Input placeholder="Password" />
      </div>
      <Button title="Register" className="auth" />
    </form>
  );
};

export default RegisterForm;
