import React from 'react';

import styles from './authentication.module.scss';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';

const Authorization = () => {
  return (
    <form className={styles.root_auth}>
      <p className={styles.text_auth}>Authorization</p>
      <div className={styles.formInput_auth}>
        <Input placeholder="E-mail" type="email" />
        <Input placeholder="Password" type="password" />
      </div>
      <Button title="log in" className="auth" />
    </form>
  );
};

export default Authorization;
