import React from 'react';

import './TestStartButton.css';

const TestStartButton = () => {
  return (
    <div className="testStartButton">
      <button type="button">Start test</button>
      <span>Test name</span>
    </div>
  );
};

export default TestStartButton;
