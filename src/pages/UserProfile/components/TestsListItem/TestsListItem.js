import React from 'react';
import PropTypes from 'prop-types';

import check from '../images/Checked.png';
import unCheck from '../images/unChecked.png';

import './TestsListItem.css';

const TestsListItem = ({ tests }) => {
  return (
    <ul className="testList">
      {tests.map(({ name, id, complete }, idx) => {
        return (
          <li key={id}>
            <span className={`imgCheck ${complete ? 'checked' : 'unChecked'}`}>
              <img src={complete ? check : unCheck} alt="checked" />
            </span>
            <span>{`${idx + 1}.`}</span>
            <span className="question">{name}</span>
          </li>
        );
      })}
    </ul>
  );
};

TestsListItem.defaultProps = {
  tests: [{ name: 'name', complete: false, id: 1 }],
};

TestsListItem.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  tests: PropTypes.array,
};

export default TestsListItem;
