import React, { useState } from 'react';
import Check from '../../pages/images/Vector29(Stroke).png';
import buttonTop from '../../pages/images/buttonTop.png';
import buttonDown from '../../pages/images/buttonDown.png';

import TestsListItem from '../TestsListItem';

// eslint-disable-next-line react/prop-types
const TestList = ({ tests }) => {
  const [showTest, setShowTest] = useState(false);

  return (
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions
    <div
      className="tests"
      onClick={() => setShowTest(!showTest)}
      onKeyPress={() => setShowTest(!showTest)}
    >
      <img className="checker" src={Check} alt="check" title="check" />
      <span className="testName">Test name: </span>
      <img className="toggleButton" src={showTest ? buttonTop : buttonDown} alt="down" />
      {showTest ? <TestsListItem tests={tests} /> : null}
    </div>
  );
};

export default TestList;
