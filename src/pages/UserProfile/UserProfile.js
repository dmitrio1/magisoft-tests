import React from 'react';
import UserAvatar from './pages/UserAvatar';
import UserInfo from './pages/UserInfo';
import UserTestResult from './pages/UserTestResult';

import styles from './userProfile.module.scss';

const UserProfile = () => {
  return (
    <div className={styles.UserProfile}>
      <section className={styles.User}>
        <UserAvatar />
        <UserInfo />
      </section>
      <section>
        <UserTestResult />
      </section>
    </div>
  );
};

export default UserProfile;
