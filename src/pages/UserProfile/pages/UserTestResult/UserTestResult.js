/* eslint-disable import/no-unresolved */
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import TestList from '../../components/TestList';

import './UserTestResult.css';
import TestStartButton from '../../components/TestStartButton';

const UserTestResult = () => {
  const [tests, setTests] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:3000/test').then(body => setTests(body.data));
  }, []);

  return (
    <div className="testsResult">
      <h2>All Test</h2>
      <TestList tests={tests} />
      <TestList tests={tests} />
      <TestStartButton />
      <TestStartButton />
    </div>
  );
};

export default UserTestResult;
