import React from 'react';
import userAvatar from '../images/avatar.png';

import './UserAvatar.scss';

const UserAvatar = () => {
  return (
    <div className="Avatar">
      <img src={userAvatar} alt="Avatar" title="avatar" />
    </div>
  );
};

export default UserAvatar;
