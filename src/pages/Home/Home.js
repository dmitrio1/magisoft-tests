import React from 'react';
import PropTypes from 'prop-types';
import styles from './home.module.scss';

const Home = props => {
  const { test } = props;
  return <div className={styles.root}>{test}</div>;
};

Home.propTypes = {
  test: PropTypes.number.isRequired,
};

export default Home;
