import React from 'react';
import { string } from 'prop-types';

import styles from './button.module.scss';

const Button = ({ title, className }) => (
  <button type="button" className={`${styles.btn} ${className ? styles[className] : ''}`}>
    {title}
  </button>
);

Button.propTypes = {
  className: string.isRequired,
  title: string.isRequired,
};

export default Button;
