import React from 'react';
import { string } from 'prop-types';

import styles from './index.module.scss';

const Input = ({ placeholder }) => (
  <input className={styles.input_form} placeholder={placeholder} />
);

Input.propTypes = {
  placeholder: string.isRequired,
};

export default Input;
